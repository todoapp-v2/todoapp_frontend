import './App.css';
import {    
  Routes,  
  Route
}   
from 'react-router-dom';  

import Hello from './Hello';



function App() {
  return (
    
    <Routes>
      <Route exact path='/hello-world' element={ <Hello /> } />
    </Routes>

  );
}

export default App;
